package intropoo.birds.bo;

public class Dove extends Bird implements CanFly {
	
	@Override
	public void describe() {
		System.out.println("I am a dove!");
	}
	
	public Dove( String name ) {
		super( name );
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Dove{" );
		sb.append( '}' );
		return sb.toString();
	}
	
	@Override
	public void fly() {
		takeOff();
		System.out.println("I fly like a dove!");
		landing();
	}
}
