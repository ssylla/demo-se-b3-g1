package intropoo.birds.bo;

public class Penguin extends Bird implements CanSwim {
	
	public Penguin( String name ) {
		super( name );
	}
	
	
	@Override
	public void describe() {
		System.out.println("I am a penguin!");
	}
	
	public void swim() {
		System.out.println("I swim like a penguin!");
	}
}
