package intropoo.birds.bo;

import java.util.Objects;

public abstract class Bird {
	
	private String name;
	
	public Bird() {
	}
	
	public abstract void describe();
	
	public Bird( String name ) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Bird{" );
		sb.append( "name='" ).append( name ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
	
	@Override
	public boolean equals( Object otherBird ) {
		if ( this == otherBird ) return true;
		if ( !(otherBird instanceof Bird) ) return false;
		Bird bird = ( Bird ) otherBird;
		return name.equals( bird.name );
	}
	
	@Override
	public int hashCode() {
		return Objects.hash( name );
	}
}
