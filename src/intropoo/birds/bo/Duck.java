package intropoo.birds.bo;

public class Duck extends Bird implements CanFly,CanSwim {
	
	public Duck( String name ) {
		super( name );
	}
	
	
	@Override
	public void describe() {
		System.out.println("I am a duck!");
	}
	
	public void swim() {
		System.out.println("I swim like à duck!");
	}
	
	@Override
	public void fly() {
		System.out.println("I fly like Donald!");
	}
}
