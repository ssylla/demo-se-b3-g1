package intropoo.birds;

import intropoo.birds.bo.*;

public class App {
	
	public static void main( String[] args ) {
		Bird[] birds = {new Dove( "A dove" ), new Duck( "Donald" )};
		
		
		for ( Bird bird : birds ) {
			bird.describe();
			if ( bird instanceof CanFly ) {
				(( CanFly ) bird).fly();
			}
			if ( bird instanceof CanSwim ) {
				(( CanSwim ) bird).swim();
			}
		}
		
		Dove d1 = new Dove( "col1" );
		Dove d2 = new Dove( "col1" );
		
		if ( d1 == d2 ) {
			System.out.println( "egalité ==" );
		} else {
			System.out.println( "non égalité ==" );
		}
		if ( d1.equals( d2 ) ) {
			System.out.println( "egalité ==" );
		} else {
			System.out.println( "non égalité ==" );
		}
	}
}
