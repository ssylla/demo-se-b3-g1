package intropoo.wrapper;

import java.lang.annotation.Inherited;

public class App {
	
	public static void main( String[] args ) {
		
		Integer i = 20;
		System.out.println( Integer.MAX_VALUE );
		
		String str = "toto";
		String str2 = "toto";
		String str3 = new String( "toto" );
		
		if ( str == str2 ) {
			System.out.println( "les str et str2 sont égales!" );
		} else {
			System.out.println( "non égales" );
		}
		if ( str.equals( str3 ) ) {
			System.out.println( "les str et str3 sont égales!" );
		} else {
			System.out.println( "non égales str et str3" );
		}
		
		
		Integer j = 20;
		
		Integer k = new Integer(20);
		
		if ( i.equals( k ) ) {
			System.out.println("egalité");
		} else {
			System.out.println("non égalité");
		}
		
		//Concat
		String hello = "bonjour";
		String finale = hello;
		String stfinale = "bonjour";
		hello = hello + " ";
		hello = hello + " la promo!";
		
		System.out.println(hello);
		if ( stfinale == finale ) {
			System.out.println("egalité");
		} else {
			System.out.println("non égalité");
		}
		
		System.err.println("Ceci est une alerte!!!");
	}
}
