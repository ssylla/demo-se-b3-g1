package intropoo.exception;

import intropoo.exception.util.DivisionParZeroException;
import intropoo.exception.util.Math;

public class App {
	
	private static void m1() throws DivisionParZeroException {
		System.out.println( "m1 begin" );
		m2();
		System.out.println( "m1 end" );
	}
	
	private static void m2() throws DivisionParZeroException {
		System.out.println( "m2 begin" );
		m3();
		System.out.println( "m2 end" );
	}
	
	private static void m3() throws DivisionParZeroException {
		System.out.println( "m3 begin" );
		int a = 100;
		int b = ( int ) (java.lang.Math.random() * 2);
		System.out.println( "Le résultat de la / entre " + a + " et " + b + " est : " + Math.division( a, b ) );
		System.out.println( "m3 end" );
	}
	
	public static void main( String[] args ) {
		System.out.println("main begin");
		try {
			//TODO ouverture ressource
			m1();
			
		} catch ( DivisionParZeroException e ) {
			System.out.println("Attention on a rencontré une / par 0 entre " + e.getDividande() + " et " + e.getDiviseur() + " !!!");
		} catch ( ArithmeticException e ) {
			System.out.println(e.getMessage());
		} finally {
			//TODO fermeture
		}
		
		System.out.println("main end");
	}
}
