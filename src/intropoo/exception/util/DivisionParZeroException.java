package intropoo.exception.util;

public class DivisionParZeroException extends Exception {
	
	private int dividande;
	private int diviseur;
	
	public DivisionParZeroException( String message ) {
		super( message );
	}
	
	public DivisionParZeroException( String message, int dividande, int diviseur ) {
		super( message );
		this.dividande = dividande;
		this.diviseur = diviseur;
	}
	
	public int getDividande() {
		return dividande;
	}
	
	public int getDiviseur() {
		return diviseur;
	}
}
