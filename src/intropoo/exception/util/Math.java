package intropoo.exception.util;

public final class Math {
	
	private Math() {}//to prevents initialization
	
	public static int division( int a, int b ) throws DivisionParZeroException {
		try {
			return a/b;
		} catch ( ArithmeticException e ) {
			throw new DivisionParZeroException( e.getMessage(), a, b );
		}
	}
}
